from argparse import ArgumentParser

from project.command import ProjectCommand


class DefaultCommand(ProjectCommand):
    """Docstring"""

    name = 'default'

    @ProjectCommand.wrap
    def execute(self):
        """Docstring"""
        self.status = f"Done with {self.config.get('setting')}"
        return f"Value {self.input.text}"
