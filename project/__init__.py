from wizlib.app import WizApp

from project.command import ProjectCommand


class ProjectApp(WizApp):

    base_command = ProjectCommand
    name = 'project'
