from tempfile import NamedTemporaryFile
from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import patch
from io import StringIO

from wizlib.input_handler import InputHandler
from wizlib.config_handler import ConfigHandler

from project import ProjectApp
from project.command.default_command import DefaultCommand


class TestCommandDefault(TestCase):

    def test_input(self):
        x = "hello"
        c = DefaultCommand(input=InputHandler.fake(x))
        n = c.execute()
        self.assertEqual(n, 'Value hello')

    def test_config(self):
        c = DefaultCommand(config=ConfigHandler.fake(setting="one"))
        n = c.execute()
        self.assertEqual(c.status, f"Done with one")

    def test_app(self):
        with patch('sys.stdout', o := StringIO()):
            with patch('sys.stderr', e := StringIO()):
                ProjectApp.run('default', debug=True)
        o.seek(0)
        self.assertTrue(o.read().startswith('Value'))
        e.seek(0)
        self.assertTrue(e.read().startswith('Done'))
