project = ''
copyright = '2023 Francis Potter'
author = 'Steampunk Wizard'
templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
extensions = ['myst_parser']
html_theme = 'bootstrap'
html_static_path = ['_static']
